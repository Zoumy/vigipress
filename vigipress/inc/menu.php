<ul class="menu-ul">
  <li class="menu-li"><a class="menu-link accueil-link" href="<?= RACINE_SITE ?>index.php">Accueil</a></li>
  <?php
  if (internauteEstConnecteEtEstAdmin()) {
    echo '<li class="menu-li"><a class="menu-link gestion-utilisateurs-link" href="' . RACINE_SITE . 'admin/gestion_utilisateur.php?action=affichage">Gestion des utilisateurs</a></li>';
  }
  if (internauteEstConnecte()) {
    echo '<li class="menu-li"><a class="menu-link profil-link" href="' . RACINE_SITE . 'admin/moncompte.php?action=affichage">Profil</a></li>';
    echo '<li class="menu-li"><a class="menu-link connexion-link" href="' . RACINE_SITE . 'admin/revue.php">Revue</a></li>';
    echo '<li class="menu-li"><a class="menu-link deconnexion-link" href="' . RACINE_SITE . 'admin/connexion.php?action=deconnexion">Se déconnecter</a></li>';
  } else {
    echo '<li class="menu-li"><a class="menu-link connexion-link" href="' . RACINE_SITE . 'admin/connexion.php">Connexion</a></li>';
    echo '<li class="menu-li"><a class="menu-link connexion-link" href="' . RACINE_SITE . 'admin/revue.php">Revue</a></li>';

  }
  ?>
</ul>