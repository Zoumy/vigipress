<?php
//Permet de savoir s'il y a une session
//C'est-à-dire si un utilisateur s'est connecté à votre site
session_start();

//Connection a notre base de données
include_once('connect.php');

//------------ Chemin / CONSTANTE
define("RACINE_SITE", "/");  
// declaration d'une constante

$content = '';

//------------ Autre inclusion
require_once("fonction.php");
?>