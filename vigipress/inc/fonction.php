<?php

//Fonction qui vérifie si l'internaute est connecté
function internauteEstConnecte() {
    return isset($_SESSION['user_id']);
}

//Fonction qui vérifie si l'internaute est connecté et est aussi administrateur
function internauteEstConnecteEtEstAdmin()
{
    return internauteEstConnecte() && isset($_SESSION['user_status']) && $_SESSION['user_status'] == 1;
}

// Redimensionné une image avec la bibliotéque et la compresser GD
function resizeAndCompressImage($uploadDir, $sourceFile, $quality = 90)
{
    list($width, $height, $type) = getimagesize($sourceFile);

    $newWidth = 64;
    $newHeight = 64;

    $destinationImage = imagecreatetruecolor($newWidth, $newHeight);

    switch ($type) {
        case IMAGETYPE_JPEG:
            $sourceImage = imagecreatefromjpeg($sourceFile);
            break;
        case IMAGETYPE_PNG:
            $sourceImage = imagecreatefrompng($sourceFile);
            break;
        default:
            return false;
    }

    imagecopyresampled($destinationImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

    $compressedFile = $uploadDir . 'compressed_' . basename($sourceFile);
    imagejpeg($destinationImage, $compressedFile, $quality);

    imagedestroy($sourceImage);
    imagedestroy($destinationImage);

    return $compressedFile;
}

function userHasPrivateRevue($pdo, $userId) {
    $query = "SELECT COUNT(*) as count FROM revue WHERE user_id = :userId AND statut = '1'";
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(':userId', $userId);
    $stmt->execute();
    $result = $stmt->fetch();
    return $result['count'] > 0;
}