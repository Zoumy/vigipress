<?php
// Connexion a la base de données

try {
    $pdo = new PDO(
        "mysql:dbname=vigipress;host=localhost",
        "root",
        "",
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
    ); //Rapport d'erreur sous forme d'exception 
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}
?>