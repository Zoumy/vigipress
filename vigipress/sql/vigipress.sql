-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 07 sep. 2023 à 13:40
-- Version du serveur : 8.0.31
-- Version de PHP : 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `vigipress`
--

-- --------------------------------------------------------

--
-- Structure de la table `press_reviews`
--

DROP TABLE IF EXISTS `press_reviews`;
CREATE TABLE IF NOT EXISTS `press_reviews` (
  `idpress_reviews` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `revue_id` int NOT NULL,
  `titre` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `photo` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpress_reviews`),
  KEY `revue_id` (`revue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `press_reviews`
--

INSERT INTO `press_reviews` (`idpress_reviews`, `user_id`, `revue_id`, `titre`, `photo`, `description`, `created_at`) VALUES
(250, 25, 23, 'grhifbrj', 'public/upload/article/compressed_dscfff.jpeg', 'grefbfeb', '2023-09-07 12:21:24');

-- --------------------------------------------------------

--
-- Structure de la table `revue`
--

DROP TABLE IF EXISTS `revue`;
CREATE TABLE IF NOT EXISTS `revue` (
  `id_revue` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `nom` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `statut` int DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_revue`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `revue`
--

INSERT INTO `revue` (`id_revue`, `user_id`, `photo`, `nom`, `description`, `statut`, `created_at`, `updated_at`) VALUES
(23, 25, '../public/upload/revue/choisir-chien-japonais.jpg', 'Chien', 'fevpb,dklb,gdklbg', 0, '2023-09-06 16:19:26', '2023-09-06 16:19:26'),
(24, 25, '../public/upload/revue/poisson-ange-francais-aquarium-la-rochelle-bloc-560X560.jpg', 'Poisson', 'vfknb dfn vfdknbvg', 1, '2023-09-06 16:22:07', '2023-09-06 16:22:07');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `iduser` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `prenom` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pseudo` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(250) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `statut` int NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`iduser`, `nom`, `prenom`, `pseudo`, `email`, `password`, `statut`) VALUES
(21, 'GAY', 'Vincent', 'Viincent', 'gay.vincent1@hotmail.com', '$2y$10$kbEaN1onSA.0CIKRdRHft.2xcMYbqcbkYXVd1N7LIJ2.Fi3BdRyx2', 1),
(23, 'Jean', 'Heude', 'JeanjeanHeudy', 'JeanjeanHeudy@hotmail.com', '$2y$10$B3xhqI.d72boKuJWzYvtG.BPWgp/25C2TfRIvkvYI4WlmeIQCjOlm', 0),
(24, 'Jean', 'Bertrand', 'Jdabnjkgbvfsf', 'villrob@gmail.com', '$2y$10$WXi4iC3yJJ4.vSKl7K9TjOjPvU7wjZkp4lqg064o31hwNEiFAjMLy', 0),
(25, 'Cordie', 'Annie', 'Cocoanne', 'Cocoanne@hotmail.com', '$2y$10$5KgoWfBQP0O.158jAp2OWeI0hwZREmrph3GFDF1EadnQUoAvs/bU2', 2);

-- --------------------------------------------------------

--
-- Structure de la table `whitelist`
--

DROP TABLE IF EXISTS `whitelist`;
CREATE TABLE IF NOT EXISTS `whitelist` (
  `id_whitelist` int NOT NULL AUTO_INCREMENT,
  `id_revue` int NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id_whitelist`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `whitelist`
--

INSERT INTO `whitelist` (`id_whitelist`, `id_revue`, `email`) VALUES
(1, 21, 'JeanjeanHeudy@hotmail.com');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `press_reviews`
--
ALTER TABLE `press_reviews`
  ADD CONSTRAINT `revue_id` FOREIGN KEY (`revue_id`) REFERENCES `revue` (`id_revue`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
