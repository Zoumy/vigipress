<?php
require "inc/head.php";
require "inc/menu.php";
?>

<div>
    <div>
        <h1 class="h1">
            <?= $titre ?>
        </h1>
    </div>
    <?= $content ?>
</div>

<?php require "inc/footer.php";