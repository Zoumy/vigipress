<?php
require 'inc/inc.php';

$titre = 'Ajout d\'article de presse';
$iduser = '';

$id_revue = (int) $_GET['id'];

// 1. Vérifiez le statut de la revue
$queryStatus = "SELECT nom, description, created_at, statut, user_id FROM revue WHERE id_revue = :id_revue";
$stmtStatus = $pdo->prepare($queryStatus);
$stmtStatus->bindParam(':id_revue', $id_revue, PDO::PARAM_INT);
$stmtStatus->execute();
$status = $stmtStatus->fetch(PDO::FETCH_ASSOC);

if (!$status) {
    die("L'article spécifiée n'existe pas.");
}

if ($status['statut'] == 1) { // Revue privée
    if (!internauteEstConnecte()) {
        die('Vous devez être connecté pour accéder à cette revue privée.');
    } else {
        // Vérifiez si l'utilisateur a l'autorisation
        $user_email = $_SESSION['email'];
        $queryWhitelist = "SELECT 1 FROM whitelist WHERE email = :user_email AND id_revue = :id_revue";
        $stmtWhitelist = $pdo->prepare($queryWhitelist);
        $stmtWhitelist->bindParam(':user_email', $user_email, PDO::PARAM_STR);
        $stmtWhitelist->bindParam(':id_revue', $id_revue, PDO::PARAM_INT);
        $stmtWhitelist->execute();
        $isWhitelisted = $stmtWhitelist->fetch(PDO::FETCH_ASSOC);

        if (!$isWhitelisted && $_SESSION['user_id'] != $status['user_id']) {
            die('Vous n\'avez pas l\'autorisation d\'accéder à cette revue privée.');
        }
    }
}

// Ajout
if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['action'] === 'Ajouter') {
    $revue_id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

    if (!$revue_id) {
        exit("L'ID de la revue est manquant ou invalide.");
    }

    $titre = trim($_POST['titre'] ?? "");
    $description = trim($_POST['description'] ?? "");
    $user_id = $_SESSION['user_id'];
    $photoPath = '';

    if (!empty($_FILES['photo']['name'])) {
        $photoPath = handlePhotoUpload();

        if (!$photoPath) {
            exit("Erreur lors du traitement de l'image.");
        }
    }

    if (addArticleToDB($titre, $description, $user_id, $revue_id, $photoPath, $pdo)) {
        echo "Article ajouté avec succès!";
    } else {
        echo "Erreur lors de l'ajout de l'article.";
    }
}

function handlePhotoUpload(): ?string
{
    $tempFile = $_FILES['photo']['tmp_name'];
    $uploadDir = "public/upload/article/";
    $targetFile = $uploadDir . basename($_FILES['photo']['name']);

    if (move_uploaded_file($tempFile, $targetFile)) {
        return resizeAndCompressImage($uploadDir, $targetFile) ?: null;
    }

    return null;
}

function addArticleToDB($titre, $description, $user_id, $revue_id, $photoPath, $pdo): bool
{
    $insertQuery = "INSERT INTO press_reviews (titre, description, user_id, revue_id, photo, created_at) 
                    VALUES (:titre, :description, :user_id, :revue_id, :photo, NOW())";

    $stmtInsert = $pdo->prepare($insertQuery);

    return $stmtInsert->execute([
        ':titre' => $titre,
        ':description' => $description,
        ':user_id' => $user_id,
        ':revue_id' => $revue_id,
        ':photo' => $photoPath
    ]);
}

// Modification
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['idpress_reviews'], $_POST['action']) && $_POST['action'] == "Modifier") {
    // Vérification de la session
    if (!isset($_SESSION['user_id'])) {
        exit("L'utilisateur n'est pas connecté.");
    }

    // Récupération des données de l'article
    $article_id = $_POST['idpress_reviews'];
    $titre = trim($_POST['titre'] ?? "");
    $description = trim($_POST['description'] ?? "");
    $user_id = $_SESSION['user_id'];

    // Validation des champs
    if (!$titre || !$description || strlen($titre) > 50 || strlen($description) > 120) {
        exit("Les données de l'article sont incomplètes ou dépassent les limites autorisées.");
    }

    $photoPath = null;
    if (isset($_FILES['photo']) && $_FILES['photo']['error'] === UPLOAD_ERR_OK) {
        $nomFichier = $_FILES['photo']['name'];
        $extensionFichier = strtolower(pathinfo($nomFichier, PATHINFO_EXTENSION));

        if (!in_array($extensionFichier, ['jpg', 'jpeg', 'png'])) {
            exit("Extension d'image non valide.");
        }

        $cheminDestination = $uploadDir . $nomFichier; 
        if (!move_uploaded_file($_FILES['photo']['tmp_name'], $cheminDestination)) {
            exit("Erreur lors du déplacement de l'image.");
        }

        $cheminCompressed = resizeAndCompressImage($uploadDir, $cheminDestination);
        if (!$cheminCompressed) {
            exit("Erreur lors de la compression de l'image.");
        }

        $photoPath = $cheminCompressed;
    }

    $query = "UPDATE press_reviews SET titre = :titre, description = :description, user_id = :user_id";
    if ($photoPath) {
        $query .= ", photo = :photo";
    }
    $query .= " WHERE idpress_reviews = :article_id";

    $stmt = $pdo->prepare($query);
    $stmt->bindParam(':titre', $titre);
    $stmt->bindParam(':description', $description);
    $stmt->bindParam(':user_id', $user_id);
    if ($photoPath) {
        $stmt->bindParam(':photo', $photoPath);
    }
    $stmt->bindParam(':article_id', $article_id);

    if (!$stmt->execute()) {
        exit("Erreur PDO : " . implode(" - ", $stmt->errorInfo()));
    }

    if ($stmt->rowCount()) {
        header('location:revue.php');
        echo "La revue a été mise à jour avec succès.";
    } else {
        echo "Aucune mise à jour effectuée.";
    }
}

// Suppresion
if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    $Delete = $_GET['id'];

        $delete = $pdo->prepare("DELETE FROM press_reviews WHERE idpress_reviews = :id");
        $delete->bindParam(':id', $Delete, PDO::PARAM_INT);
        $delete->execute();
}

// Récupérer l'ID de l'URL
// $id_revue = (int) $_GET['id'];

// Récupérer l'action de tri de l'URL (si présente)
$actionTri = isset($_GET['tri']) ? $_GET['tri'] : 'recent';

// Mettre à jour la requête SQL en fonction de l'ordre de tri sélectionné
if ($actionTri === 'recent') {
    $query = "SELECT * FROM press_reviews ORDER BY created_at DESC";
} else {
    $query = "SELECT * FROM press_reviews ORDER BY created_at ASC";
}

// Le formulaire de tri avec ID et actionTri
$content .= '<form method="GET">';
$content .= '<label for="tri">Trier par :</label>';
$content .= '<select name="tri" id="tri">';
$content .= '<option value="recent" ' . ($actionTri === 'recent' ? 'selected' : '') . '>Date récente</option>';
$content .= '<option value="ancien" ' . ($actionTri === 'ancien' ? 'selected' : '') . '>Date ancienne</option>';
$content .= '</select>';
$content .= '<input type="hidden" name="id" value="' . $id_revue . '">'; // Conserver l'ID dans le formulaire
$content .= '<input type="submit" value="Trier">';
$content .= '</form><br>';

if (internauteEstConnecte()) {
    $iduser = $_SESSION['user_id'];
    // Bouton pour afficher le formulaire
    $content .= '<button id="toggle_form_article">Ajouter un article</button><br><br>';
    // Formulaire d'ajout avec un ID pour le cibler avec JavaScript
    $content .= '<form action="" method="POST" enctype="multipart/form-data" id="add_revue_form" style="display: none;">';

    $content .= '<div>';
    $content .= '<label for="titre">Titre:</label>';
    $content .= '<input type="text" id="titre" name="titre" required><br><br>';
    $content .= '</div>';
    $content .= '<div>';
    $content .= '<label for="photo">Photo:</label>';
    $content .= '<input type="file" id="photo" name="photo"><br><br>';
    $content .= '</div>';
    $content .= ' <div>';
    $content .= '<label for="description">Description:</label>';
    $content .= '<textarea id="description" name="description" required></textarea><br><br>';
    $content .= '</div>';
    $content .= '<div>';
    $content .= '<input type="hidden" name="revue_id" value="add"' . $id_revue . '">';
    $content .= '<input type="submit" name="action"  value="Ajouter"><br><br>';
    $content .= '</div>';
    $content .= '</form>';
}

// Récupérer la valeur de tri du formulaire
$tri = isset($_GET['tri']) ? $_GET['tri'] : 'recent';

// Jointure pour récupérer la revue et ses articles de presse associés
$query = "SELECT * FROM press_reviews WHERE revue_id = :id_revue";

// Mettre à jour la requête SQL en fonction de l'ordre de tri sélectionné
if ($tri === 'recent') {
    $query .= " ORDER BY created_at DESC";
} else {
    $query .= " ORDER BY created_at ASC";
}

$stmt = $pdo->prepare($query);
$stmt->bindParam(':id_revue', $id_revue, PDO::PARAM_INT);
$stmt->execute();

$reviews = $stmt->fetchAll(PDO::FETCH_ASSOC);

if ($reviews) {
    // Afficher la revue
    $content .= '<h3>' . htmlspecialchars($reviews[0]['titre']) . '</h3>';

    $content .= '<table>';
    $content .= '<thead>';
    $content .= '<tr>';
    $content .= '<th>Photo</th>';
    $content .= '<th>Titre</th>';
    $content .= '<th>Description</th>';
    $content .= '<th>Date</th>';
    $content .= '<th>Actions</th>'; // Ajout de la colonne pour les actions
    $content .= '</tr>';
    $content .= '</thead>';
    $content .= '<tbody>';

    foreach ($reviews as $review) {
        $content .= '<tr>';
        $content .= '<td><a href="detail.php?id=' . $review['idpress_reviews'] . '"><img src="' . $review['photo'] . '" alt="Photo de ' . $review['titre'] . '" width="100"></a></td>';
        $content .= '<td><a href="detail.php?id=' . $review['idpress_reviews'] . '">' . htmlspecialchars($review['titre']) . '</a></td>';
        $content .= '<td>' . htmlspecialchars($review['description']) . '</td>';
        $content .= '<td>' . date("d/m/Y H:i", strtotime($review['created_at'])) . '</td>';

        $content .= '<td>' . htmlspecialchars($review['description']) . '</td>';
        // Vérification de l'ID utilisateur pour afficher le bouton de modification

        if (isset($review['user_id']) && isset($_SESSION['user_id']) && $_SESSION['user_id'] == $review['user_id']) {
            $content .= '<td><a href="#" class="toggle_edit_form" data-id="' . $review['user_id'] . '">Modifier</a></td>';
            $content .= '<td><a href="article_revue.php?action=delete&id=' . $review['idpress_reviews'] . ';">Supprimer</a></td>';
        } else {
            $content .= '<td></td>';
        }
        $content .= '</tr>';

        // Formulaire pour modification
        $content .= '<tr class="edit-form-row" id="modifierProfilFormRow-' . $review['user_id'] . '" style="display: none;">';
        $content .= '<td colspan="5">';

        // Ouverture du formulaire
        $content .= '<form class="edit-form" action="" method="post" enctype="multipart/form-data">';

        // Champ caché pour l'ID de l'article
        $content .= '<input type="hidden" name="id_revue" value="' . $review['idpress_reviews'] . '">';

        // Champ pour le titre
        $content .= '<label for="titre-' . $review['idpress_reviews'] . '">Titre:</label>';
        $content .= '<input type="text" id="titre-' . $review['idpress_reviews'] . '" name="titre" value="' . htmlspecialchars($review['titre']) . '"><br>';

        // Champ pour la description
        $content .= '<label for="description-' . $review['idpress_reviews'] . '">Description:</label>';
        $content .= '<textarea id="description-' . $review['idpress_reviews'] . '" name="description">' . htmlspecialchars($review['description']) . '</textarea><br>';

        // Champ pour l'upload de photo
        $content .= '<label for="photo-' . $review['idpress_reviews'] . '">Photo actuelle :</label>';
        $content .= '<img src="' . htmlspecialchars($review['photo']) . '" alt="' . htmlspecialchars($review['titre']) . '" width="64"><br><br>';
        $content .= '<label for="photo-' . $review['idpress_reviews'] . '">Modifier la photo :</label>';
        $content .= '<input type="file" id="photo-' . $review['idpress_reviews'] . '" name="photo" accept="image/jpeg, image/png"><br><br>';

        // Bouton de soumission du formulaire
        $content .= '<input type="submit" name="action" value="Modifier">';

        // Fermeture du formulaire
        $content .= '</form>';
        $content .= '</td>';
        $content .= '</tr>';


    }

    $content .= '</tbody>';
    $content .= '</table>';

} else {
    echo "La revue spécifiée n'existe pas ou n'a pas de revues de presse associées.";
}

$content .= <<<JS

JS;

$content .= <<<JS
<script>
if (document.getElementById('toggle_form_article')) { // Vérifiez d'abord si l'élément existe
    // Ajout article formulaire caché
        document.getElementById('toggle_form_article').addEventListener('click', function () {
            var form = document.getElementById('add_revue_form');
            if (form.style.display === 'none' || form.style.display === "") {
                form.style.display = 'block';
                this.textContent = 'Fermer le formulaire';
            } else {
                form.style.display = 'none';
                this.textContent = 'Ajouter un article';
            }
        });
    }
</script>
JS;

require "template.php";
?>

<script>
    // Modifier les événements pour tous les boutons "Modifier"
    document.querySelectorAll(".toggle_edit_form").forEach(function (btn) {
        btn.addEventListener("click", function (event) {
            event.preventDefault();  // empêche le comportement par défaut du lien

            // Récupérer l'ID de la revue depuis l'attribut data-id du bouton
            var reviewId = btn.getAttribute("data-id");

            // Trouver la ligne de formulaire associée
            var formRow = document.getElementById("modifierProfilFormRow-" + reviewId);

            // Basculer l'affichage de la ligne de formulaire
            if (formRow.style.display === "none" || formRow.style.display === "") {
                formRow.style.display = "table-row";
            } else {
                formRow.style.display = "none";
            }
        });
    });
</script>