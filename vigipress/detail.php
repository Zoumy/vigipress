<?php
require 'inc/inc.php';

$titre = 'Article';

// Vérification de l'ID de l'article
$articleId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
var_dump($articleId);

if (!$articleId) {
    $content .= '<p>Identifiant de l\'article invalide.</p>';
} else {
    // Requête SQL pour récupérer les détails de l'article
    $getArticle = $pdo->prepare("SELECT * FROM press_reviews WHERE idpress_reviews = :article_id");
    $getArticle->bindValue(':article_id', $articleId, PDO::PARAM_INT);
    $getArticle->execute();
    $articleDetails = $getArticle->fetch(PDO::FETCH_ASSOC);

    if (!$articleDetails) {
        $content .= '<p>Article non trouvé.</p>';
    } else {
        // Affichage des détails de l'article
        $content .= '<h2 class="titre-article">' . htmlspecialchars($articleDetails['titre']) . '</h2>';
        $formattedDate = date('d.m.Y H:i:s', strtotime($articleDetails['created_at']));
        $content .= '<p class="date-publication">Date de publication : ' . $formattedDate . '</p>';
        $content .= '<p class="description-article">' . htmlspecialchars($articleDetails['description']) . '</p>';

        // Vérification de la disponibilité de la photo
        if (!empty($articleDetails['photo'])) {
            $content .= '<img class="image-article" src="' . htmlspecialchars($articleDetails['photo']) . '" alt="' . htmlspecialchars($articleDetails['titre'], ENT_QUOTES, 'UTF-8') . '">';
        }

        // Vous pouvez également ajouter un lien de retour à la revue ici si nécessaire
    }
}

// Le reste de votre code

require "template.php";
?>