<?php
require 'inc/inc.php';

// ------------------------------ Variables ------------------------------

$titre = 'Accueil';
$imageCount = 0; // Initialisez le compteur d'images
$articles ='';

// ------------------------------ Fin Variables ------------------------------

// ------------------------------ Requête SQL ------------------------------
if (internauteEstConnecte()) {
    $iduser = $_SESSION['user_id'];
    $user_email = $_SESSION['email'];

    $query = "SELECT r.* 
    FROM revue r
    WHERE r.statut = 0 OR (r.statut = 1 AND r.id_revue IN 
        (SELECT id_revue FROM whitelist WHERE email = :user_email) 
    OR r.user_id = :iduser)";

    $stmt = $pdo->prepare($query);
    $stmt->bindParam(':iduser', $iduser, PDO::PARAM_INT);
    $stmt->bindParam(':user_email', $user_email, PDO::PARAM_STR);
    $stmt->execute();
    $revues = $stmt->fetchAll();
} else {
    // Si l'utilisateur n'est pas connecté, ne récupérez que les revues publiques
    $query = "SELECT * FROM revue WHERE statut = '0'";
    $revues = $pdo->query($query)->fetchAll();
}


// ------------------------------ Fin Requête SQL ------------------------------

// ------------------------------ Boucles PHP ------------------------------

// Afficher la liste des articles récents
$content .= '<ul class="article-list">';

foreach ($revues as $revue) {
    $content .= '<table>';
    $content .= '<tr>';
    $content .= '<td class="img-container"><a href="../article_revue.php?id=' . $revue['id_revue'] . '"><img width="" src=' . htmlspecialchars($revue['photo']) . ' alt=' . htmlspecialchars($revue['nom']) . '></a></td>';
    $content .= '<td><a href="../article_revue.php?id=' . $revue['id_revue'] . '">' . htmlspecialchars($revue['nom']) . "</a></td>";
    $formattedDate = date("d/m/Y H:i", strtotime($revue['created_at']));
    $content .= '<td>' . htmlspecialchars($formattedDate) . '</td>';
    $content .= '<td class="' . ($revue['statut'] == '0' ? "statut-public" : "statut-prive") . '">' . ($revue['statut'] == '0' ? "Public" : "Privé") . '</td>';    
    $imageCount++;
    if ($imageCount % 4 === 0) {
        $content .= '</div>';
    }
    $content .= '</table>';
}


if ($imageCount % 4 !== 0) {
    $content .= '</div>';
}
$content .= '</ul>';

// ------------------------------ Fin Boucles PHP ------------------------------

require "template.php";
?>