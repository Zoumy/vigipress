<?php
require '../inc/inc.php';

$titre = 'Ajout de Revue';
$uploadDir = '../public/upload/revue/'; // Dossier d'upload
$iduser = '';

// *1
if (internauteEstConnecte()) {
    $iduser = $_SESSION['user_id'];
    $user_email = $_SESSION['email'];

    $query = "SELECT * FROM revue WHERE statut = 0 OR (statut = 1 AND id_revue IN (SELECT id_revue FROM whitelist WHERE email = :user_email) OR user_id = :iduser)";

    $stmt = $pdo->prepare($query);
    $stmt->bindParam(':iduser', $iduser, PDO::PARAM_INT);
    $stmt->bindParam(':user_email', $user_email, PDO::PARAM_STR);
    $stmt->execute();
    $revues = $stmt->fetchAll();
} else {
    // Si l'utilisateur n'est pas connecté, ne récupérez que les revues publiques
    $query = "SELECT * FROM revue WHERE statut = '0'";
    $revues = $pdo->query($query)->fetchAll();
}

// Formulaire ajout
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_FILES['photo'])) {
    $nomRevue = $_POST['nom'];
    $descriptionRevue = $_POST['description'];

    // Valider la longueur du nom et de la description
    if (strlen($nomRevue) > 50) {
        $erreur = "Le nom ne doit pas dépasser 50 caractères.";
    } elseif (strlen($descriptionRevue) > 120) {
        $erreur = "La description ne doit pas dépasser 120 caractères.";
    } else {
        $nomFichier = $_FILES['photo']['name'];
        $cheminTemporaire = $_FILES['photo']['tmp_name'];

        // Vérifie le type de fichier 
        $extensionsAutorisees = ['jpg', 'jpeg', 'png'];
        $extensionFichier = strtolower(pathinfo($nomFichier, PATHINFO_EXTENSION));

        if (!in_array($extensionFichier, $extensionsAutorisees)) {
            $erreur = "Seuls les fichiers JPG, JPEG et PNG sont autorisés.";
        } else {
            // Renommer le fichier téléchargé avec son nom d'origine
            $cheminDestination = $uploadDir . $nomFichier;

            if (move_uploaded_file($cheminTemporaire, $cheminDestination)) {
                $statutRevue = $_POST['statut'];

                // Requête SQL d'insertion *2
                $insertRevue = "INSERT INTO revue (photo, nom, description, statut, user_id, created_at, updated_at) VALUES (?, ?, ?, ?, ?, NOW(), NOW())";

                $insert_Revue = $pdo->prepare($insertRevue);
                $insert_Revue->execute([$cheminDestination, $nomRevue, $descriptionRevue, $statutRevue, $iduser]);

                // Récupérez l'ID de la dernière revue insérée
                $id_revue = $pdo->lastInsertId();

                if ($insert_Revue) {
                    $message = "L'image a été téléchargée avec succès :" . $nomFichier . "La revue a été créée.";
                } else {
                    $erreur = "Une erreur s'est produite lors de l'insertion de la revue dans la base de données.";
                }
            } else {
                $erreur = "Une erreur s'est produite lors du téléchargement du fichier.";
            }
        }
    }
}

// Suppresion
// Vérifie si l'ID de la revue est défini et est un nombre
if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    // Récupère l'ID de la revue
    $revue = $_GET['id_revue'];

    // Utilisation d'une requête préparée pour DELETE  *4
    $stmt = $pdo->prepare("DELETE FROM revue WHERE id_revue = :id_revue");
    $stmt->bindParam(':id_revue', $revue, PDO::PARAM_INT);
    $stmt->execute();
}

// Formulaire modification requete *3
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['id_revue'], $_POST['action']) && $_POST['action'] == "Modifier") {

    $id_revue = $_POST['id_revue'];
    $nomRevue = $_POST['nom'];
    $descriptionRevue = $_POST['description'];
    $statutRevue = $_POST['statut'];

    if (strlen($nomRevue) <= 50 && strlen($descriptionRevue) <= 120) {

        $updateRevue = "UPDATE revue SET nom = ?, description = ?, statut = ?, updated_at = NOW()";
        $params = [$nomRevue, $descriptionRevue, $statutRevue];

        // Traitement de l'image
        if (isset($_FILES['photo']) && $_FILES['photo']['error'] === UPLOAD_ERR_OK) {
            $nomFichier = $_FILES['photo']['name'];
            $extensionFichier = strtolower(pathinfo($nomFichier, PATHINFO_EXTENSION));

            if (in_array($extensionFichier, ['jpg', 'jpeg', 'png'])) {
                $cheminDestination = $uploadDir . $nomFichier;
                if (move_uploaded_file($_FILES['photo']['tmp_name'], $cheminDestination)) {

                    // Appel à la fonction resizeAndCompressImage
                    $cheminCompressed = resizeAndCompressImage($uploadDir, $cheminDestination);

                    // Si la fonction retourne un chemin valide
                    if ($cheminCompressed) {
                        $updateRevue .= ", photo = ?";
                        $params[] = $cheminCompressed;
                    } else {
                        die("Erreur lors de la compression de l'image.");
                    }

                } else {
                    die("Erreur lors du déplacement de l'image.");
                }
            } else {
                die("Extension d'image non valide.");
            }
        }

        $updateRevue .= " WHERE id_revue = ? AND user_id = ?";
        $params[] = $id_revue;
        $params[] = $iduser;

        $stmt = $pdo->prepare($updateRevue);
        $stmt->execute($params);

        if ($stmt->errorInfo()[1]) {
            die("Erreur PDO : " . $stmt->errorInfo()[2]);
        }

        if ($stmt->rowCount()) {
            header('location:revue.php');
            echo "La revue a été mise à jour avec succès.";
        } else {
            echo "Aucune mise à jour effectuée.";
        }
    } else {
        echo "Erreur dans la validation.";
    }
}

// Récupérer la valeur de tri du formulaire
$tri = isset($_GET['tri']) ? $_GET['tri'] : 'recent';

// Requête SQL de base pour sélectionner les revues en fonction du statut
$query = "SELECT * FROM revue WHERE statut = 0 OR (statut = 1 AND (id_revue IN (SELECT id_revue FROM whitelist WHERE email = :user_email) OR user_id = :iduser))";

// Mettre à jour la requête SQL en fonction de l'ordre de tri sélectionné
if ($tri === 'recent') {
    $query .= " ORDER BY created_at DESC";
} else {
    $query .= " ORDER BY created_at ASC";
}

// Préparez et exécutez la requête SQL en utilisant PDO
$stmt = $pdo->prepare($query);
$stmt->bindParam(':user_email', $user_email, PDO::PARAM_STR);
$stmt->bindParam(':iduser', $iduser, PDO::PARAM_INT);
$stmt->execute();

// Récupérez les revues triées
$revues = $stmt->fetchAll();


$content .= '<form method="GET">';
$content .= '<label for="tri">Trier par :</label>';
$content .= '<select name="tri" id="tri">';
$content .= '<option value="recent" ' . ($tri === 'recent' ? 'selected' : '') . '>Date récente</option>';
$content .= '<option value="ancien" ' . ($tri === 'ancien' ? 'selected' : '') . '>Date ancienne</option>';
$content .= '</select>';
$content .= '<input type="submit" value="Trier">';
$content .= '</form><br>';


if (internauteEstConnecte()) {
    // Bouton pour afficher le formulaire
    $content .= '<button id="toggle_form_revue">Ajouter une revue</button><br><br>';

    // Formulaire pour ajouter une revue
    $content .= '<div id="add_revue_form" style="display: none;">';
    $content .= '<form action="" method="post" enctype="multipart/form-data">';
    $content .= '<label for="nom">Nom :</label>';
    $content .= '<input type="text" id="nom" name="nom" placeholder="Max 50 caractères" maxlength="50" required><br><br>';
    $content .= '<label for="description">Description :</label>';
    $content .= '<textarea id="description" name="description" rows="4" cols="50" placeholder="Max 120 caractères" maxlength="120" required></textarea><br><br>';
    $content .= '<label for="photo">Photo :</label>';
    $content .= '<input type="file" id="photo" name="photo" accept="image/jpeg, image/png" required><br><br>';
    $content .= '<label for="statut">Statut :</label>';
    $content .= '<select id="statut" name="statut" required>';
    $content .= '<option value="0">Public</option>';
    $content .= '<option value="1">Privé</option>';
    $content .= '</select><br><br>';
    $content .= '<input type="submit" value="Ajouter la revue">';
    $content .= '</form>';
    $content .= '</div>';
}
// Construction du tableau pour afficher les revues
$content .= '<table>';
$content .= '<tr class="cacher">';
$content .= '<th>Photo</th>';
$content .= '<th>Nom</th>';
$content .= '<th>Description</th>';
$content .= '<th>Date de création</th>';
$content .= '<th>Statut</th>';
$content .= '<th>Action</th>'; // Ajoutez une colonne pour l'action (Modifier)
$content .= '</tr>';

// Boucle pour afficher les revues triées par ordre chronologique
foreach ($revues as $revue) {
    $content .= '<tr>';
    $content .= '<td><a href="../article_revue.php?id=' . $revue['id_revue'] . '"><img src=' . htmlspecialchars($revue['photo']) . ' alt=' . htmlspecialchars($revue['nom']) . ' width="64" height="64"><a></td>';
    $content .= '<td><a href="../article_revue.php?id=' . $revue['id_revue'] . '">' . htmlspecialchars($revue['nom']) . "</a></td>";
    $content .= '<td>' . htmlspecialchars($revue['description']) . '</td>';
    $formattedDate = date("d/m/Y H:i", strtotime($revue['created_at']));
    $content .= '<td>' . htmlspecialchars($formattedDate) . '</td>';
    $content .= '<td class="' . ($revue['statut'] == '0' ? "statut-public" : "statut-prive") . '">' . ($revue['statut'] == '0' ? "Public" : "Privé") . '</td>';

    // Condition pour afficher le bouton Modifier uniquement si l'utilisateur a publié la revue (statut = 1) *5
    if ($revue['user_id'] == $iduser) {
        // Affichage du bouton modifier
        $content .= '<td><a href="#" class="btn-modifier" data-id="' . $revue['id_revue'] . '">Modifier</a></td>';
        $content .= '<td><a href="revue.php?action=delete&id_revue=' . $revue['id_revue'] . '" class="delete_review" action="delete" data-id="' . $revue['id_revue'] . '">Supprimer</a></td>';
    } else {
        $content .= '<td></td>'; // Sinon, laissez la cellule vide
    }

    // Formulaire de modification (cachée)
    if ($revue['user_id'] == $iduser) {
        $content .= '<tr class="modif-form-row" id="modif-form-' . $revue['id_revue'] . '" style="display: none;">';
        $content .= '<td colspan="6">'; // colspan pour étendre le formulaire sur la largeur de la table
        $content .= '<form action="" method="post" enctype="multipart/form-data">'; // Ajout de enctype pour le téléchargement de fichiers
        $content .= '<input type="hidden" name="id_revue" value="' . $revue['id_revue'] . '">'; // Pour savoir quelle revue est en cours de modification
        $content .= '<label for="nom-' . $revue['id_revue'] . '">Nom :</label>';
        $content .= '<input type="text" id="nom-' . $revue['id_revue'] . '" name="nom" value="' . htmlspecialchars($revue['nom']) . '"><br><br>';
        $content .= '<label for="description-' . $revue['id_revue'] . '">Description :</label>';
        $content .= '<textarea id="description-' . $revue['id_revue'] . '" name="description">' . htmlspecialchars($revue['description']) . '</textarea><br><br>';
        $content .= '<label for="photo-' . $revue['id_revue'] . '">Photo actuelle :</label>';
        $content .= '<img src="' . htmlspecialchars($revue['photo']) . '" alt="' . htmlspecialchars($revue['nom']) . '" width="64"><br><br>';
        $content .= '<label for="photo-' . $revue['id_revue'] . '">Modifier la photo :</label>';
        $content .= '<input type="file" id="photo-' . $revue['id_revue'] . '" name="photo" accept="image/jpeg, image/png"><br><br>';
        $content .= '<label for="statut-' . $revue['id_revue'] . '">Statut :</label>';
        $content .= '<select id="statut-' . $revue['id_revue'] . '" name="statut">';
        $content .= '<option value="0"' . ($revue['statut'] == '0' ? ' selected' : '') . '>Public</option>';
        $content .= '<option value="1"' . ($revue['statut'] == '1' ? ' selected' : '') . '>Privé</option>';
        $content .= '</select><br><br>';
        $content .= '<input type="submit" name="action" value="Modifier">';
        $content .= '</form>';
        $content .= '</td>';
        $content .= '</tr>';
    }

    $content .= '</tr>';
}
$content .= '</table>';

require "../template.php";

?>

<script>
    document.addEventListener('DOMContentLoaded', function () {

        // Toggle le formulaire d'ajout de revue
        const toggleButton = document.getElementById('toggle_form_revue');
        const revueForm = document.getElementById('add_revue_form');

        if (toggleButton && revueForm) {
            toggleButton.addEventListener('click', function () {
                if (revueForm.style.display === 'none' || revueForm.style.display === '') {
                    revueForm.style.display = 'block';
                } else {
                    revueForm.style.display = 'none';
                }
            });
        }

        document.querySelectorAll('.btn-modifier').forEach(btn => {
            btn.addEventListener('click', function (e) {
                e.preventDefault();

                // Récupérer l'ID de la revue à partir du bouton cliqué
                const idRevue = btn.getAttribute('data-id');

                // Trouver le formulaire associé à cet ID
                const formRow = document.getElementById('modif-form-' + idRevue);

                // Basculer l'affichage du formulaire
                if (formRow.style.display === 'none' || formRow.style.display === '') {
                    formRow.style.display = 'table-row';  
                } else {
                    formRow.style.display = 'none';
                }
            });
        });

    });
</script>