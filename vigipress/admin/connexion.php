<?php
require '../inc/inc.php';

// ------------------------------ Variables ------------------------------

$titre = 'Connexion';

// ------------------------------ Fin Variables ------------------------------

// ------------------------------ Boucle PHP ------------------------------

if (isset($_GET['action']) && $_GET['action'] == "deconnexion") {
    session_destroy();
    header("location:connexion.php");
    exit();
}

if (internauteEstConnecte()) {
    header("location:moncompte.php");
    exit();
}

if (isset($_POST['connexion'])) {
    $verif_caractere = preg_match(
        '#^[-\w\.]+@([-a-z0-9]+\.)+[a-z]$#',
        $_POST['email']
    );

    if (!$verif_caractere && (strlen($_POST['email']) < 1 || strlen($_POST['email']) > 150)) {
        $contenu .= "<div class='erreur'> Le format de l'email n'est pas valide. Exemple: exemple@exemple.com</div>";
    } else {
        $resultat = $pdo->prepare("SELECT iduser, password, statut FROM users WHERE email = :email");
        $resultat->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $resultat->execute();

        if ($resultat->rowCount() != 0) {
            $user = $resultat->fetch(PDO::FETCH_ASSOC);
    
            if (password_verify($_POST['password'], $user['password'])) {
                $_SESSION['user_id'] = $user['iduser'];
                $_SESSION['user_status'] = $user['statut'];
                $_SESSION['email'] = $_POST['email']; 
                header("location:moncompte.php");
                exit();
            } else {
                $content .= '<div class="erreur">Mot de passe erroné</div>';
            }
        } else {
            $content .= '<div class="erreur">Erreur d\'email</div>';
        }
    }
}

// ------------------------------ Fin Boucles PHP ------------------------------

// ------------------------------ HTML ------------------------------

$content .= '<form class="form-connexion" action="" method="post" onsubmit="return validateConnexion()';
$content .= '<label class="lab-connexion" for="email">E-mail :*</label>';
$content .= '<input class="text-connexion" type="text" name="email" required>';
$content .= '<span id="error_email" class="error"></span><br>';

$content .= '<label class="lab-connexion" for="password">Mot de passe :*</label>';
$content .= '<input class="text-connexion" type="password" name="password" required>';
$content .= '<span id="error_password" class="error"></span><br>';

$content .= '<input class="connexion" type="submit" name="connexion" value="Connexion">';
$content .= '<ul class="ul-connexion">';
$content .= '<li><a class="a-connexion" href="inscription.php">Inscription</a></li>';
$content .= '</ul>';
$content .= '</form>';

// ------------------------------ Fin HTML ------------------------------

require "../template.php";
?>

<script>
    function validateConnexion() {
        var isValid = true;

        var email = document.getElementById("email").value;
        var error_email = document.getElementById("error_email");
        if (!/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/
            /.test(newEmail)) {
            errorNewEmail.innerHTML = "Veuillez entrer une adresse email valide.";
            isValid = false;
        } else {
            errorNewEmail.innerHTML = "";
        }

        var password = document.getElementById("password").value;
        var error_password = document.getElementById("error_password");
        if (!/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,}$/.test(password)) {
            error_password.innerHTML = "Veuillez entrer un mot de passe valide.";
            isValid = false;
        } else {
            error_password.innerHTML = "";
        }
        return isValid; 
    }

</script>