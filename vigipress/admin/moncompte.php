<?php
require '../inc/inc.php';

// ------------------------------ Variables ------------------------------

$titre = 'Mon Compte';
$content = ''; // Initialisation de la variable $content
$userId['id'] = $_SESSION['user_id'];
$user['email'] = $_SESSION['email'];
$hasPrivateReviews = false;
$showWhitelistForm = false; 

// ------------------------------ Fin Variables ------------------------------

// ------------------------------ Requête SQL ------------------------------

$user = $pdo->prepare("SELECT nom, prenom, pseudo, statut, email FROM users WHERE iduser = :iduser"); // Selectionne l'utilisateur

$update = $pdo->prepare("UPDATE users SET nom = :nom, prenom = :prenom, pseudo = :pseudo, email = :email, statut = :statut WHERE iduser = :iduser");
// Mêtre a jour l'utilisateur

// Récupérez les revues de cet utilisateur
$query = "SELECT * FROM revue WHERE user_id = :userId";
$stmt = $pdo->prepare($query);
$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
$stmt->execute();
$revues = $stmt->fetchAll();

$query = "SELECT * FROM revue WHERE user_id = ? AND statut = '1'";
$stmt = $pdo->prepare($query);
$stmt->execute([$_SESSION['user_id']]);
$privateRevues = $stmt->fetchAll();
// var_dump($_SESSION);


// ------------------------------ Fin Requête SQL ------------------------------

// ------------------------------ Boucle PHP ------------------------------
// ------------------------------ HTML ------------------------------

// Si l'utilisateur est connecté
if (isset($_SESSION['email']) && isset($_SESSION['user_id'])) {
    $userId = $_SESSION['user_id'];

    // Récupérez les revues privées de cet utilisateur
    $checkPrivateRevueQuery = "SELECT COUNT(*) as private_count FROM revue WHERE user_id = ? AND statut = '1'";
    $stmtPrivateCheck = $pdo->prepare($checkPrivateRevueQuery);
    $stmtPrivateCheck->execute([$userId]);
    $resultPrivateCheck = $stmtPrivateCheck->fetch();

    if ($resultPrivateCheck['private_count'] > 0) {
        $hasPrivateReviews = true;
    }
}

if(isset($_SESSION['user_id']) && userHasPrivateRevue($pdo, $_SESSION['user_id'])) {
    $showWhitelistForm = true;
}


// Si le formulaire est soumis
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['whitelist_email']) && isset($_POST['revue'])) {
    $emailToAdd = $_POST['whitelist_email'];
    $revueId = $_POST['revue'];

    // Vérification du statut de la revue
    $checkRevueStatus = "SELECT statut FROM revue WHERE id_revue = ?";
    $stmtCheck = $pdo->prepare($checkRevueStatus);
    $stmtCheck->execute([$revueId]);
    $revueData = $stmtCheck->fetch();

    if ($revueData['statut'] == '1') { // Si la revue est privée
        $addToWhitelistQuery = "INSERT INTO whitelist (id_revue, email) VALUES (?, ?)";
        $stmtWhitelist = $pdo->prepare($addToWhitelistQuery);
        $stmtWhitelist->execute([$revueId, $emailToAdd]);
        if ($stmtWhitelist) {
            $message = "L'e-mail a été ajouté à la liste blanche pour cette revue.";
        } else {
            $erreur = "Une erreur s'est produite lors de l'ajout à la liste blanche.";
        }
    } else {
        $erreur = "Vous ne pouvez pas ajouter un e-mail à la liste blanche pour une revue publique.";
    }
}

if (internauteEstConnecte()) {
    $iduser = $_SESSION['user_id'];
    $user->bindValue(':iduser', $iduser);
    $user->execute();
    $users = $user->fetch(PDO::FETCH_ASSOC);
    
    if ($users) {
        $content .= '<div class="profil-section"';
        $content .= '<p class="info">Nom : ' . $users['nom'] . '</p>';
        $content .= '<p class="info">Prénom : ' . $users['prenom'] . '</p>';
        $content .= '<p class="info">Identifiant : ' . $users['pseudo'] . '</p>';
        $content .= '<p class="info">E-mail : ' . $users['email'] . '</p>';
        $content .= '<button id="modifierProfilBtn">Modifier profil</button>';
        $content .= '</div>';

        // Formulaire de modification des informations (caché)
        $content .= '<div class="form-container" id="modifierProfilForm" style="display: none;">';
        $content .= '<h2>Modifier les informations</h2>';
        $content .= '<form action="" method="post" onsubmit="return validateForm()">';

        // Formulaire d'information caché
        $content .= '<label for="new_nom">Modification nom :</label>';
        $content .= '<input type="text" name="new_nom" pattern="^[A-Za-z\s]+$" value="' . $users['nom'] . '" title="Le nom doit contenir uniquement des lettres et des espaces." required><br>';
        $content .= '<span id="error_new_nom" class="error"></span><br>';

        $content .= '<label for="new_prenom">Modification prénom :</label>';
        $content .= '<input type="text" name="new_prenom" pattern="^[A-Za-z\s]+$" value="' . $users['prenom'] . '" title="Le prénom doit contenir uniquement des lettres et des espaces." required><br>';
        $content .= '<span id="error_new_prenom" class="error"></span><br>';

        $content .= '<label for="new_pseudo">Modification pseudo :</label>';
        $content .= '<input type="text" name="new_pseudo" pattern="^[A-Za-z0-9_]+$" value="' . $users['pseudo'] . '" title="Le pseudo ne peut contenir que des lettres, des chiffres et le caractère underscore." required><br>';
        $content .= '<span id="error_new_pseudo" class="error"></span><br>';

        $content .= '<label for="new_email">Modification email :</label>';
        $content .= '<input type="email" name="new_email" value="' . $users['email'] . '" title="Veuillez entrer une adresse email valide." required><br>';
        $content .= '<span id="error_new_email" class="error"></span><br>';

        $content .= '<label for="new_hidden">Utilisateur caché :</label>';
        $content .= '<input type="checkbox" name="new_hidden" value="1" ' . ($users['statut'] == 2 ? 'checked' : '') . '><br>';

        $content .= '<input type="submit" name="modifier" value="Modifier" onclick="validateForm()">';
        $content .= '</form>';
        $content .= '<a class="mot_de_passe_lien" href="#">Modifier le mot de passe</a>';
        $content .= '</div>';

        if ($showWhitelistForm) {
            $content .= 'Formulaire de whitelist';
            $content .= '<div>';
            $content .= '<form action="" method="post">';
            $content .= '<label for="revue">Sélectionnez vos revue privée:</label>';
            $content .= '<select id="revue" name="revue">';
            foreach ($privateRevues as $revue) {
                $content .= '<option value=' . $revue['id_revue'] . '>' . htmlspecialchars($revue['nom']) . '</option>';
            }
            $content .= '</select><br><br>';
            
            $content .= '<label for="whitelist_email">E-mail à ajouter :</label>';
            $content .= '<input type="email" id="whitelist_email" name="whitelist_email" required><br><br>';
            
            $content .= '<input type="submit" value="Ajouter à la liste blanche">';
            $content .= '</form>';
            $content .= '</div>';
        }        


        $content .= '</div>';
        // ------------------------------ Fin HTML ------------------------------

        // Traitement de la modification
        if (isset($_POST['modifier'])) {
            $newNom = $_POST['new_nom'];
            $newPrenom = $_POST['new_prenom'];
            $newPseudo = $_POST['new_pseudo'];
            $newEmail = $_POST['new_email'];
            $newStatut = isset($_POST['new_hidden']) ? 2 : 0; // Si la case est cochée, statut caché (2), sinon utilisateur normal (0)

            if (!preg_match("/^[A-Za-z\s]+$/u", $newNom)) {
                $errors[] = "Le nom doit contenir uniquement des lettres et des espaces.";
            }

            if (!preg_match("/^[A-Za-z\s]+$/u", $newPrenom)) {
                $errors[] = "Le prénom doit contenir uniquement des lettres et des espaces.";
            }

            if (!preg_match("/^[A-Za-z0-9_]+$/", $newPseudo)) {
                $errors[] = "Le pseudo ne peut contenir que des lettres, des chiffres et le caractère underscore.";
            }

            $update->bindValue(':nom', $newNom);
            $update->bindValue(':prenom', $newPrenom);
            $update->bindValue(':pseudo', $newPseudo);
            $update->bindValue(':email', $newEmail);
            $update->bindValue(':statut', $newStatut); // Mettre à jour le statut en fonction de la case à cocher
            $update->bindValue(':iduser', $iduser);

            try {
                $update->execute();
                header("Location: moncompte.php");
                exit();
            } catch (PDOException $e) {
                $content .= "Erreur lors de la modification : " . $e->getMessage();
            }
        }
    } else {
        $content .= 'Utilisateur introuvable.';
    }
} else {
    $content .= 'Vous devez être connecté pour accéder à cette page.';
}

// ------------------------------ Fin Boucle PHP ------------------------------

require "../template.php";
?>

<!-- ------------------------------ Script JavaScript ------------------------------ -->

<!-- Script JavaScript pour contrôler la visibilité du formulaire -->
<script>
    var modifierProfilForm = document.getElementById("modifierProfilForm");
    var modifierProfilBtn = document.getElementById("modifierProfilBtn");
    modifierProfilBtn.addEventListener("click", function () {
        if (modifierProfilForm.style.display === "none") {
            modifierProfilForm.style.display = "block";
        } else {
            modifierProfilForm.style.display = "none";
        }
    });
</script>

<!-- Script message erreur  -->
<script>
    function validateForm() {
        var isValid = true;

        var newNom = document.getElementById("new_nom").value;
        var errorNewNom = document.getElementById("error_new_nom");
        if (!/^[A-Za-z\s]+$/.test(newNom)) {
            errorNewNom.innerHTML = "Le nom doit contenir uniquement des lettres et des espaces.";
            isValid = false;
        } else {
            errorNewNom.innerHTML = "";
        }

        var newPrenom = document.getElementById("new_prenom").value;
        var errorNewPrenom = document.getElementById("error_new_prenom");
        if (!/^[A-Za-z\s]+$/.test(newPrenom)) {
            errorNewPrenom.innerHTML = "Le prénom doit contenir uniquement des lettres et des espaces.";
            isValid = false;
        } else {
            errorNewPrenom.innerHTML = "";
        }

        var newPseudo = document.getElementById("new_pseudo").value;
        var errorNewPseudo = document.getElementById("error_new_pseudo");
        if (!/^[A-Za-z0-9_]+$/.test(newPseudo)) {
            errorNewPseudo.innerHTML = "Le pseudo ne peut contenir que des lettres, des chiffres et le caractère underscore.";
            isValid = false;
        } else {
            errorNewPseudo.innerHTML = "";
        }

        var newEmail = document.getElementById("new_email").value;
        var errorNewEmail = document.getElementById("error_new_email");
        if (!/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(newEmail)) {
            errorNewEmail.innerHTML = "Veuillez entrer une adresse email valide.";
            isValid = false;
        } else {
            errorNewEmail.innerHTML = "";
        }
        return isValid;
    }
</script>

<!--  ------------------------------ Fin Script JavaScript ------------------------------ -->