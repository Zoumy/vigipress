<?php
require '../inc/inc.php';

// ------------------------------ Variables ------------------------------

$titre = 'Gestion des Utilisateurs';

// ------------------------------ Fin Variables ------------------------------
// ------------------------------ Requête SQL ------------------------------

$user = $pdo->prepare("SELECT iduser, nom, prenom, pseudo, email, statut FROM users WHERE statut IN (0, 1, 2)"); // Récupération utilisateur



// ------------------------------ Fin Requête SQL ------------------------------

// ------------------------------ Boucle PHP ------------------------------
if (isset($_POST['submit_action'])) {
    $user_id = $_POST['user_id'];
    $action = $_POST['action'];
    
    if (isset($_POST['submit_action'])) {
        $user_id = $_POST['user_id'];
        $action = $_POST['action'];
        
        if ($action === 'modifier_statut') {
            $newStatus = ($_POST['new_status'] == 'admin') ? 1 : 0; // Assume que la valeur 'admin' signifie le statut d'administrateur
            // Mettez à jour le statut de l'utilisateur dans la base de données
            $updateStatus = $pdo->prepare("UPDATE users SET statut = :statut WHERE iduser = :iduser");
            try {
                $updateStatus = $pdo->prepare("UPDATE users SET statut = :statut WHERE iduser = :iduser"); // Modification du statut Admin/Utilisateur
                $updateStatus->bindValue(':statut', $newStatus);
                $updateStatus->bindValue(':iduser', $user_id);
                $updateStatus->execute();
                header("Location: gestion_utilisateur.php");
                exit();
            } catch (PDOException $e) {
                $content .= "Erreur lors de la mise à jour du statut : " . $e->getMessage();
            }
        } elseif ($action === 'supprimer') {
        }
    }
}

if (isset($_POST['delete_user'])) {
    $user_id = $_POST['user_id'];

    // Supprimer l'utilisateur de la base de données
    try {
        $delete = $pdo->prepare("DELETE FROM users WHERE iduser = :iduser"); // Suppression utilisateur
        $delete->bindValue(':iduser', $user_id);
        $delete->execute();
        
        header("Location: gestion_utilisateur.php");
        exit();
    } catch (PDOException $e) {
        $content .= "Erreur lors de la suppression de l'utilisateur : " . $e->getMessage();
    }
}
// ------------------------------ HTML ------------------------------

if (internauteEstConnecteEtEstAdmin()) {
    // Récupérer la liste de tous les utilisateurs
    $user->execute();
    $users = $user->fetchAll(PDO::FETCH_ASSOC);
    
    if ($users) {
        $content .= '<table class="user-table">';
        $content .= '<tr>';
        $content .= '<th>ID</th>';
        $content .= '<th>Nom</th>';
        $content .= '<th>Prénom</th>';
        $content .= '<th>Pseudo</th>';
        $content .= '<th>Email</th>';
        $content .= '<th>Statut</th>';
        $content .= '<th>Action</th>';
        $content .= '</tr>';

        foreach ($users as $user) {
            $content .= '<tr>';
            $content .= '<td>' . $user['iduser'] . '</td>';
            $content .= '<td>' . $user['nom'] . '</td>';
            $content .= '<td>' . $user['prenom'] . '</td>';
            $content .= '<td>' . $user['pseudo'] . '</td>';
            $content .= '<td>' . $user['email'] . '</td>';
            $content .= '<td>' . ($user['statut'] == 1 ? 'Admin' : ($user['statut'] == 2 ? 'Utilisateur caché' : 'Utilisateur')) . '</td>';
            $content .= '<td>';
            $content .= '<form action="" method="post">';
            $content .= '<input type="hidden" name="user_id" value="' . $user['iduser'] . '">';
            $content .= '<select name="new_status" class="status-select" onchange="this.form.submit()">';
            $content .= '<option value="0" ' . ($user['statut'] == 0 ? 'selected' : '') . '>Utilisateur</option>';
            $content .= '<option value="1" ' . ($user['statut'] == 1 ? 'selected' : '') . '>Admin</option>';
            $content .= '<option value="2" ' . ($user['statut'] == 2 ? 'selected' : '') . '>Utilisateur caché</option>';
            $content .= '</select>';
            $content .= '<input type="submit" name="delete_user" class="delete-button" value="Supprimer">';
            $content .= '</form>';
            $content .= '</td>';
            $content .= '</tr>';
        
            // Vérifier si le formulaire a été soumis pour cette ligne
            if (isset($_POST['user_id']) && $_POST['user_id'] == $user['iduser']) {
                $newStatus = $_POST['new_status'];
                try {
                    $updateStatus->bindValue(':statut', $newStatus);
                    $updateStatus->bindValue(':iduser', $user['iduser']);
                    $updateStatus->execute();
                    // Rediriger vers la page après la mise à jour
                    header("Location: gestion_utilisateur.php");
                    exit();
                } catch (PDOException $e) {
                    $content .= "Erreur lors de la mise à jour du statut : " . $e->getMessage();
                }
            }
        }        
        $content .= '</table>';
    } else {
        $content .= 'Aucun utilisateur trouvé.';
    }
} else {
    $content .= 'Accès restreint. Vous devez être connecté en tant qu\'administrateur pour accéder à cette page.';
}


// ------------------------------ Fin HTML ------------------------------
// ------------------------------ Fin Boucles PHP ------------------------------

require "../template.php";
?>