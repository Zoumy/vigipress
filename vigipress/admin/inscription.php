<?php
require '../inc/inc.php';

// ------------------------------ Variables ------------------------------

$titre = 'Inscription';
$errors = array();

// ------------------------------ Fin Variables ------------------------------

// ------------------------------ Requête SQL ------------------------------


// ------------------------------ Fin SQL ------------------------------

// ------------------------------ Boucle PHP ------------------------------
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['submit'])) {
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $pseudo = $_POST['pseudo'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        $emailPattern = '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/';

        $passwordPattern = '/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,}$/';

        // Vérifier si le formulaire est valide
        if (!preg_match("/^[A-Za-z\s]+$/u", $nom)) {
            $errors[] = "Le nom doit contenir uniquement des lettres et des espaces.";
        }
    
        if (!preg_match("/^[A-Za-z\s]+$/u", $prenom)) {
            $errors[] = "Le prénom doit contenir uniquement des lettres et des espaces.";
        }
        
        if (!preg_match("/^[A-Za-z0-9_]+$/", $pseudo)) {
            $errors[] = "Le pseudo ne peut contenir que des lettres, des chiffres et le caractère underscore.";
        }
    
        if (!preg_match($emailPattern, $email)) {
            $errors[] = "Veuillez saisir une adresse email valide.";
        }
    
        if (!preg_match($passwordPattern, $password)) {
            $errors[] = "Le mot de passe doit contenir au moins 8 caractères, dont au moins une lettre majuscule, une lettre minuscule et un chiffre.";
        }
    
        // Si le formulaire est valide, insérer les données dans la base de données
        if (count($errors) == 0) {
            // Exécuter la requête
            try {
                $user = "INSERT INTO users (nom, prenom, pseudo, email, password) VALUES (:nom, :prenom, :pseudo, :email, :password)"; // Inséré utilisateur
                $users = $pdo->prepare($user);
                $users->bindValue(':nom', $nom);
                $users->bindValue(':prenom', $prenom);
                $users->bindValue(':pseudo', $pseudo);
                $users->bindValue(':email', $email);
                $users->bindValue(':password', password_hash($password, PASSWORD_BCRYPT));
                $users->execute();
    
                // Enregistrer le message de succès dans une variable de session
                $_SESSION['success_message'] = "Compte créé avec succès.";
    
            } catch (PDOException $e) {
                echo "Erreur pour créer le compte : " . $e->getMessage();
            }
        } else {
            // Afficher les erreurs
            foreach ($errors as $error) {
                echo $error . "<br>";
            }
        }
    }
}

// ------------------------------ Script JavaScript ------------------------------

// Si un message de succès est enregistré dans la session, l'afficher
if (isset($_SESSION['success_message'])) {
    $content .= '<p>' . $_SESSION['success_message'] . '</p>';
    unset($_SESSION['success_message']); // Supprimer le message de la session
    // Redirection automatique vers la page de connexion après 3 secondes
    $content .= '<script>
        setTimeout(function() {
            window.location.href = "connexion.php";
        }, 3000);
    </script>';
}
// ------------------------------ Fin Script JavaScript ------------------------------
// ------------------------------ Fin Boucle PHP ------------------------------

// ------------------------------ HTML ------------------------------

$content .= '<form class ="form-inscription" action="" method="post">';
$content .= '<label class="label-inscription" for="nom">Nom :</label>';
$content .= '<input class="form-inscripion" placeholder="Minuscules et majuscules" type="text" name="nom" required>';
$content .= '<label class="label-inscription" for="prenom">Prénom :</label>';
$content .= '<input class="form-inscripion" placeholder="Minuscules et majuscules" type="text" name="prenom" required>';
$content .= '<label class="label-inscription" for="pseudo">Pseudo :</label>';
$content .= '<input class="form-inscripion" placeholder="Minuscules, majuscules et chiffres" type="text" name="pseudo" required>';
$content .= '<label class="label-inscription" for="email">Email :</label>';
$content .= '<input class="form-inscripion" placeholder="E-mail valide" type="email" name="email" required>';
$content .= '<label class="label-inscription" for="password">Mot de passe :</label>';
$content .= '<input class="form-inscripion" type="password" name="password" required>';
$content .= "<p class='password'>(Le mot de passe doit contenir au moins 8 caractères, dont au moins une lettre majuscule, une lettre minuscule et un chiffre.)</p>";
$content .= '<input class="creer-compte" type="submit" name="submit" value="Créer le compte">';
$content .= '</form>';

// ------------------------------ Fin HTML ------------------------------
?>

<?php
require "../template.php";
?>